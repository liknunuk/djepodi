
<div class="container">
  <div class="row">
    <div class="col-lg-12">
        <form action="<?=BASEURL;?>Home/setMychild" method="post" class="form-horizontal">
            <input type="hidden" name="gen" value="<?=$data['gen'];?>">
            <input type="hidden" name="parentId" value="<?=$data['ortu'];?>">
            <div class="form-group row">
                <label class="col-lg-3" for="ortu">Nama Orang Tua</label>
                <div class="col-lg-9">
                    <input type="text" name="ortu" id="ortu" class="form-control" value="<?=$data['namaortu']['nama'];?>" readonly >
                </div>
            </div>

            <div class="form-group row">
                <div class="label col-lg-3">Nama Lengkap</div>
                <div class="col-lg-9">
                    <input type="text" name="fullname" id="fullname" class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <div class="label col-lg-3">Tempat Lahir</div>
                <div class="col-lg-9">
                    <input type="text" name="pob" id="pob" class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <div class="label col-lg-3">Tanggal lahir</div>
                <div class="col-lg-9">
                    <input type="date" name="dob" id="dob" class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <div class="label col-lg-3">Tempat tinggal</div>
                <div class="col-lg-9">
                    <input type="text" name="homeAddr" id="homeAddr" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success float-right">Simpan</button>
            </div>

        </form>
    </div>
  </div>
</div>

<?php $this->view('template/bs4js'); ?>
