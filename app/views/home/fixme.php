
<div class="container">
  <?php $this->view('home/banner'); ?>
  <div class="row mt-5">
    <div class="col-lg-12">
        
        <form action="<?=BASEURL;?>Home/setmeup" method="post" class="form-horizontal">
          <input type="hidden" name="gen" id="iam_gen" class="form-control" value="<?=$data['iam']['gen'];?>" >
          <input type="hidden" name="familyId" id="iam_familyId" class="form-control" value="<?=$data['iam']['familyId'];?>">
          <input type="hidden" name="parentId" id="iam_parentId" class="form-control" value="<?=$data['iam']['parentId'];?>">
          <div class="form-group row">
              <label for="fullname" class="col-lg-3">Nama Lengkap</label>
              <div class="col-lg-9">
                  <input type="text" name="fullname" id="iam_fullname" class="form-control" value="<?=$data['iam']['fullname'];?>">
              </div>
          </div>
          <div class="form-group row">
              <label for="pob" class="col-lg-3">Tempat Lahir</label>
              <div class="col-lg-9">
                  <input type="text" name="pob" id="iam_pob" class="form-control" value="<?=$data['iam']['pob'];?>">
              </div>
          </div>
          <div class="form-group row">
              <label for="dob" class="col-lg-3">Tanggal Lahir</label>
              <div class="col-lg-9">
                  <input type="date" name="dob" id="iam_dob" class="form-control" value="<?=$data['iam']['dob'];?>" required>
              </div>
          </div>
          <div class="form-group row">
              <label for="homeAddr" class="col-lg-3">Alamat Rumah</label>
              <div class="col-lg-9">
                  <input type="text" name="homeAddr" id="iam_homeAddr" class="form-control" value="<?=$data['iam']['homeAddr'];?>">
              </div>
          </div>
          <div class="form-group row">
              <label for="phone" class="col-lg-3">No. Telp/HP</label>
              <div class="col-lg-9">
                  <input type="text" name="phone" id="iam_phone" class="form-control" value="<?=$data['iam']['phone'];?>">
              </div>
          </div>
          <!-- <div class="form-group row">
              <label for="passedDate" class="col-lg-3">passedDate</label>
              <div class="col-lg-9">
                  <input type="text" name="passedDate" id="iam_passedDate" class="form-control" value="">
              </div>
          </div> -->
          <div class="form-group row">
              <label for="matchType" class="col-lg-3">Pasangan</label>
              <div class="col-lg-9">
                  <select name="matchType" id="iam_matchType" class="form-control">
                  <option value="">Belum berpasangan</option>
                  <option value="Suami">Suami</option>
                  <option value="Istri">Istri</option>
                  </select>
              </div>
          </div>
          <div class="form-group row">
              <label for="matchName" class="col-lg-3">Nama Pasangan (<?=$data['iam']['matchName'];?>)</label>
              <div class="col-lg-9">
                  <input type="text" name="matchName" id="iam_matchName" class="form-control" value="<?=$data['iam']['matchName'];?>">
              </div>
          </div>
          <div class="form-group row">
              <label for="matchOrigin" class="col-lg-3">Asal Pasangan</label>
              <div class="col-lg-9">
                  <input type="text" name="matchOrigin" id="iam_matchOrigin" class="form-control" value="<?=$data['iam']['matchOrigin'];?>">
              </div>
          </div>
          <div class="form-group row">
              <label for="matchParent" class="col-lg-3">Orang tua pasangan</label>
              <div class="col-lg-9">
                  <input type="text" name="matchParent" id="iam_matchParent" class="form-control" value="<?=$data['iam']['matchParent'];?>">
              </div>
          </div>
          <div class="form-group row">
              <label for="matchPhone" class="col-lg-3">No. Telp Pasangan</label>
              <div class="col-lg-9">
                  <input type="text" name="matchPhone" id="iam_matchPhone" class="form-control" value="<?=$data['iam']['matchPhone'];?>">
              </div>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-success float-right">Simpan</button>
          </div>        
        </form>
    </div>    
  </div>
</div>

<?php $this->view('template/bs4js'); ?>
