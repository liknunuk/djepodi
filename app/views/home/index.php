<!-- 
  Sample view wntuk web page body
  
  mengambil variabel dari controlers
  $xxx = $data['elemen']

  Alternatif looping memanfaatkan $data
  
  < ?php foreach($data['xxx'] AS $xxx): ? >
  // data /konten yang diualang
  < ?php endforeach; ?>

-->
<div class="container">
  <?php $this->view('home/banner'); ?>
  <div class="row">
    <div class="col-lg-12">
        <h3 class="text-center px-3 mb-5 mt-3">
            Dinasti Djemiah ~ Supono ~ Karnadi Purboyuwono
        </h3>
        <div class="table-responsive">
            <div class="text-center">
              <h4>Anak-anak: <?=$data['ortu'];?></h4>
            </div>
            <table class="table table-sm table-striped">
              <tbody>
              <?php foreach($data['anak'] as $anak ): ?>
                <tr>
                  
                  <td><?=$anak['fullname'];?></td>
                  <td class='text-right px-3'>
                    <a class="btn btn-primary btn-sm" href="<?=BASEURL;?>Home/iam/<?=$anak['familyId'];?>">Detil</a>
                    <?php $nxgen = $data['gen'] + 1; ?>
                    <a href="<?=BASEURL;?>Home/<?=sprintf('%02d',$nxgen);?>/<?=$anak['familyId'];?>" class="btn btn-primary btn-sm">Keturunan</a>
                  </td>
                </tr>
              <?php endforeach;?>
              </tbody>
            </table>
            <div class="text-center">
                <a href="<?=BASEURL;?>" ><i class="fa fa-home" style="font-size:32px;" ></i></a>
            </div>
        </div>
    </div>    
  </div>
</div>

<?php $this->view('template/bs4js'); ?>
