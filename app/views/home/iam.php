
<div class="container">
<?php $this->view('home/banner'); ?>
  <div class="row mt-3">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-sm table-striped">
              <tbody>
                <tr>
                    <th>Family ID</th>
                    <td>G<?=$data['person']['gen'];?>-<?=$data['person']['familyId'];?>-<?=$data['person']['parentId'];?></td>
                </tr>
                <tr>
                    <th>Nama</th>
                    <td><?=$data['person']['fullname'];?></td>
                </tr>
                <tr>
                    <th>Kelahiran</th>
                    <td><?=$data['person']['pob'];?>, <?=$data['person']['dob'];?></td>
                </tr>
                <tr>
                    <th>Alamat Tinggal</th>
                    <td><?=$data['person']['homeAddr'];?></td>
                </tr>
                <tr>
                    <th>No. Telp</th>
                    <td><?=$data['person']['phone'];?></td>
                </tr>
                <tr>
                    <td colspan="2" class='text-center'><strong>PASANGAN</strong></td>
                </tr>
                <tr>
                    <th>Nama pasangan (<?=$data['person']['matchType'];?>)</th>
                    <td><?=$data['person']['matchName'];?></td>
                </tr>
                <tr>
                    <th>Telp. <?=$data['person']['matchType'];?></th>
                    <td><?=$data['person']['matchPhone'];?></td>
                </tr>
                <tr>
                    <th>Asal <?=$data['person']['matchType'];?></th>
                    <td><?=$data['person']['matchOrigin'];?></td>
                </tr>
                <tr>
                    <th>Orang Tua <?=$data['person']['matchType'];?></th>
                    <td><?=$data['person']['matchParent'];?></td>
                </tr>
              </tbody>
            </table>
            <div class="text-center">
                <a href="<?=BASEURL;?>Home/childMe/<?=$data['person']['gen'].'/'.$data['person']['familyId'];?>" class="btn btn-primary">+Anak</a>
                <a href="<?=BASEURL;?>Home/fixMe/<?=$data['person']['familyId'];?>" class="btn btn-primary">Edit</a>
                <?php
                $parentGen = sprintf('%02d',$data['person']['gen'] - 1);

                ?>
                <a href="<?=BASEURL . $data['person']['gen'] . '/' . $data['person']['parentId'];?>" class="btn btn-success">Kembali</a>
                
            </div>
        </div>
    
    </div>    
  </div>

  <div class="row mt-5">
      <div class="col-lg-12">
        <?php if($data['person']['matchType'] !== ''): ?>
            <h4 class="text-center">Anak dan Menantu</h4>
            <table class="table table-sm table-striped">
                <thead>
                    <tr>
                        <th>Anak</th>
                        <th>Menantu</th>
                    </tr>
                </thead>
                <tbody id="anakmantu">
                    <?php foreach($data['anakmantu'] as $aman): ?>
                    <tr>
                        <td><?=$aman['fullname'];?><br/><?=$aman['pob'];?>, <?=$aman['dob'];?></td>
                        <td><?=$aman['matchName'];?> (<?=$aman['matchType'];?>) </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        <?php endif; ?>
        <div class="text-center">
            <a href="<?=BASEURL . 'Home/shreder/' . $data['person']['familyId'];?>" class="btn btn-danger">Hapus</a>
        </div>
      </div>
  </div>
</div>

<?php $this->view('template/bs4js'); ?>
