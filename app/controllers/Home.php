<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Home extends Controller
{
  // method default
  public function index($gen='01',$pid='000')
  {
    $data['title'] = "Dinasti Djemiah";

    $data['anak'] = $this->model('Model_corefam')->genPertama($gen,$pid);
    if($pid=='000'){
      $ortu['nama'] = "Ibu Djemiah";
    }else{
      $ortu = $this->model('Model_corefam')->myname($pid);
    }

    $data['gen'] = $gen;
    $data['pid'] = $pid;
    $data['ortu'] = $ortu['nama'];

    $this->view('template/header',$data);
    $this->view('home/index',$data);
    $this->view('template/footer');
  }
  
  public function iam($fid){
    $data['title'] = "Dinasti Djemiah";
    $data['person'] = $this->model('Model_corefam')->iam($fid);
    $data['anakmantu'] = $this->model('Model_corefam')->anakmantu($fid);
    
    $this->view('template/header',$data);
    $this->view('home/iam',$data);
    $this->view('template/footer');
  }
  
  public function fixme($fid){
    
    $data['title'] = "Dinasti Djemiah";
    $data['iam'] = $this->model('Model_corefam')->iam($fid);
    
    $this->view('template/header',$data);
    $this->view('home/fixme',$data);
    $this->view('template/footer');

  }

  public function childMe($gen,$ortu){
    $data['title'] = "Keturunan Djemiah";
    $data['gen'] = sprintf("%02d", ($gen + 1));
    $data['ortu'] = $ortu;
    $data['namaortu'] = $this->model('Model_corefam')->myname($ortu);
    
    $this->view('template/header',$data);
    $this->view('home/anak',$data);
    $this->view('template/footer');
  }

  

  public function setmeup(){
    if($this->model('Model_corefam')->fixme( $_POST) > 0 ) header("Location:" . BASEURL . "Home/iam/" . $_POST['familyId']);
  }
  
  public function setMychild(){
    if($this->model('Model_corefam')->setFamily( $_POST) > 0 ) header("Location:" . BASEURL . "Home/iam/" . $_POST['parentId']);
  }

  public function shreder($fid){
    if( $this->model('Model_corefam')->divorced($fid) > 0 ){
      header("Location:" . BASEURL . 'Home');
    }
  }
}
