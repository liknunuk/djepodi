<?php
class Model_corefam
{
    private $table = "coreFamily";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    
    public function genPertama($gen,$pid){

        $sql = "SELECT gen, familyId , fullname FROM " . $this->table . " WHERE gen = :gen && parentId = :pid ORDER BY familyId";
        $this->db->query($sql);
        $this->db->bind('gen',$gen);
        $this->db->bind('pid',$pid);
        return $this->db->resultSet();

    }

    public function iam($fid){
        $sql = "SELECT * FROM coreFamily WHERE familyId=:fid";
        $this->db->query($sql);
        $this->db->bind('fid',$fid);
        $this->db->execute();
        return $this->db->resultOne();
    }

    public function fixme($data){
        $sql =" UPDATE " . $this->table . " SET gen=:gen,parentId=:parentId,fullname=:fullname,pob=:pob,dob=:dob,homeAddr=:homeAddr,phone=:phone,matchType=:matchType,matchName=:matchName,matchOrigin=:matchOrigin,matchParent=:matchParent,matchPhone=:matchPhone WHERE familyId=:familyId";
        
        $this->db->query($sql);
        $this->db->bind('gen',$data['gen']);
        $this->db->bind('parentId',$data['parentId']);
        $this->db->bind('fullname',$data['fullname']);
        $this->db->bind('pob',$data['pob']);
        $this->db->bind('dob',$data['dob']);
        $this->db->bind('homeAddr',$data['homeAddr']);
        $this->db->bind('phone',$data['phone']);
        $this->db->bind('matchType',$data['matchType']);
        $this->db->bind('matchName',$data['matchName']);
        $this->db->bind('matchOrigin',$data['matchOrigin']);
        $this->db->bind('matchParent',$data['matchParent']);
        $this->db->bind('matchPhone',$data['matchPhone']);
        $this->db->bind('familyId',$data['familyId']);

        $this->db->execute();
        return $this->db->rowCount();
    }

    public function myname($fid){
        
        $sql = "SELECT fullname nama FROM " . $this->table . " WHERE familyId=:fid";
        $this->db->query($sql);
        $this->db->bind('fid',$fid);
        return $this->db->resultOne();

    }

    public function setFamily($data){
        
        $sql = "INSERT INTO coreFamily SET gen=:gen , parentId=:parentId , fullname=:fullname , pob=:pob , dob=:dob , homeAddr=:homeAddr";
        $this->db->query($sql);
        $this->db->bind('gen',$data['gen']);
        $this->db->bind('parentId',$data['parentId']);
        $this->db->bind('fullname',$data['fullname']);
        $this->db->bind('pob',$data['pob']);
        $this->db->bind('dob',$data['dob']);
        $this->db->bind('homeAddr',$data['homeAddr']);
        $this->db->execute();
        return $this->db->rowCount();

    }

    public function anakmantu($fid){

        $sql = "SELECT fullname, pob, dob , matchType , matchName FROM coreFamily WHERE parentId=:fid ORDER BY dob";
        $this->db->query($sql);
        $this->db->bind('fid',$fid);
        return $this->db->resultSet();

    }

    public function divorced($fid){
        
        $sql = "DELETE FROM " . $this->table . " WHERE familyId=:fid";
        $this->db->query($sql);
        $this->db->bind('fid',$fid);
        $this->db->execute();
        return $this->db->rowCount();

    }

}

/*
    $sql = "";
    $this->db->query($sql);
    $this->db->bind('xxx',$xxx);
    $this->db->execute();
    return $this->db->resultSet();
    return $this->db->resultOne();
    return $this->db->rowCount();
*/